import boto3
def instances_state(ins_id):
    ec2_client = boto3.client('ec2', region_name = 'ap-south-1')
    ids = ec2_client.describe_instances()
    for x in ids['Reservations']:
        for y in x['Instances']:
            if (y['InstanceId']==ins_id):
                return y['State']['Name']