import boto3
from instanceState import instances_state
from requests import get
def get_instance_id():
    id_list = []
    index = 0
    ec2_client = boto3.client('ec2', region_name = 'ap-south-1')
    ids = ec2_client.describe_instances()
    for x in ids['Reservations']:
        for y in x['Instances']:
            id_list.append(y['InstanceId'])
            print('{index} instance id = {a} in state {b}'.format(a=y['InstanceId'], b=y['State']['Name'], index =index))
            index = index + 1
    index_id = int(input('Enter index value to terminate instance : '))
    ins_id = id_list[index_id]
    response = ec2_client.terminate_instances(InstanceIds=[ins_id])
    while len(id_list)!=0:
        if(instances_state(ins_id)=='shutting-down'):
            print('Shutting Down._.')
            print('Shutting Down.-.')
        else:
            print('Terminated')
            break
get_instance_id()