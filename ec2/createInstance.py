import boto3
def create_instance():
    ec2_client = boto3.client('ec2', region_name='ap-south-1')
    instances = ec2_client.run_instances(
        ImageId = "ami-0d2986f2e8c0f7d01",
        MinCount = 1,
        MaxCount = 1,
        InstanceType = "t2.micro",
    )
    print(instances)
    instanceid = (instances["Instances"][0]["InstanceId"])
    return instanceid

instance_id = create_instance()