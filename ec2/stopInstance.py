import boto3

def stop_instance():
    ec2 = boto3.client('ec2')
    response = ec2.describe_instances()
    index = 0
    id_list = []
    for data in response['Reservations']:
        for data2 in data['Instances']:
            if(data2['State']['Name'] == 'running'):
                if(stop_choice == 2):
                    response = ec2.stop_instances(InstanceIds=[data2['InstanceId']])
                    print('stopping {}'.format(data2['InstanceId']))
                elif(stop_choice == 1):
                    id_list.append(data2['InstanceId'])
                    print('enter {index} to stop {a} instance at {b} state'.format(index=index, a=data2['InstanceId'], b=data2['State']['Name']))
                    index = index + 1
    if(stop_choice == 1 and len(id_list)!=0):
        index = int(input('Enter Value :'))
        response = ec2.stop_instances(InstanceIds=[id_list[index]])
    else:
        print('there are no running instances')

stop_choice = int(input('''
Enter 1 for Stop Single Instance
Enter 2 for Stop All instances
:'''))
stop_instance()
