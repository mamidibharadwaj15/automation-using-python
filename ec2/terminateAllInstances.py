import boto3
from instanceState import instances_state

def terminate_all():
    ec2_client = boto3.client('ec2')
    resource = ec2_client.describe_instances()
    for data in resource['Reservations']:
        for ins_data in data['Instances']:
            if(instances_state(ins_data['InstanceId']) == 'running' or instances_state(ins_data['InstanceId']) == 'stopped'):
                resource = ec2_client.terminate_instances(InstanceIds=[ins_data['InstanceId']])
terminate_all()